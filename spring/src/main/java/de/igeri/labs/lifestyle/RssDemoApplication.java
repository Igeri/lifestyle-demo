package de.igeri.labs.lifestyle;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
public class RssDemoApplication {

    public static void main(final String[] args) {
        SpringApplication.run(RssDemoApplication.class, args);
    }

    @Bean
    @Scope("prototype")
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    @Controller
    static class MvcController {
        @RequestMapping({"", "/", "/index.html"})
        public String index() {
            return "index";
        }
    }

}
