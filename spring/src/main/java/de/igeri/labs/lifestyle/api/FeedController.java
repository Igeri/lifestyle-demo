package de.igeri.labs.lifestyle.api;

import de.igeri.labs.lifestyle.domain.Feed;
import de.igeri.labs.lifestyle.domain.FeedType;
import de.igeri.labs.lifestyle.domain.Post;
import de.igeri.labs.lifestyle.service.FeedService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.util.Collection;
import java.util.Map;

@RestController
@RequestMapping("/api/feeds")
public class FeedController {

    private final FeedService feedService;

    public FeedController(final FeedService feedService) {
        this.feedService = feedService;
    }

    @PutMapping
    public ResponseEntity<Void> addFeed(@RequestBody final Map<String, String> data)
        throws URISyntaxException, MalformedURLException {
        final String url = data.get("url");
        final boolean result = this.feedService.addFeed(url, FeedType.RSS2);

        if (result) {
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping
    public ResponseEntity<Collection<Feed>> listFeeds() {
        final Collection<Feed> feeds = this.feedService.list();

        return new ResponseEntity<>(feeds, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> removeFeed(@PathVariable final Long id){
        if (id == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        this.feedService.remove(id);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/{id}/posts")
    public ResponseEntity<Collection<Post>> listPosts(@PathVariable("id") final Long feedId) {
        if (feedId == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        final Collection<Post> posts = this.feedService.list(feedId);
        return new ResponseEntity<>(posts, HttpStatus.OK);
    }

}
