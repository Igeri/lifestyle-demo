package de.igeri.labs.lifestyle.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.net.URI;

@Entity
@Data
@NoArgsConstructor
@EqualsAndHashCode(exclude = "id")
public class Feed {

    @Id
    @GeneratedValue
    private Long id;
    private URI uri;
    private String title;
    @Enumerated(EnumType.STRING)
    private FeedType type;

}
