package de.igeri.labs.lifestyle.domain;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import de.igeri.labs.lifestyle.feed.Rss2;

import java.util.Collection;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public enum FeedType {
    RSS2(Rss2.class, () -> new XmlMapper()
        .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)) {
        @Override
        public Collection<Post> mapPosts(final Object rawFeed) {
            if (rawFeed instanceof Rss2) {
                final Rss2 rss = (Rss2) rawFeed;
                final Rss2.Channel channel = rss.getChannel();
                final Rss2.Item[] items = channel.getItems();

                return Stream.of(items)
                    .map(this::mapPost)
                    .sorted()
                    .collect(Collectors.toSet());
            }
            throw new IllegalArgumentException("Cannot map unknown feed class!");
        }

        private Post mapPost(final Rss2.Item item) {
            final Post post = new Post();

            post.setTitle(item.getTitle());
            post.setGuid(item.getGuid());
            post.setLink(item.getLink());
//            post.setDop(new Date(item.getPubDate().getNano()));

            return post;
        }

        @Override
        public Feed mapFeed(final Object rawFeed) {
            // TODO move to Rss2 class?
            if (rawFeed instanceof Rss2) {
                final Rss2 rss = (Rss2) rawFeed;
                final Rss2.Channel channel = rss.getChannel();

                final Feed feed = new Feed();
                feed.setType(FeedType.RSS2);
                feed.setUri(channel.getLink());
                feed.setTitle(channel.getTitle());

                return feed;
            }
            throw new IllegalArgumentException("Cannot map unknown feed class!");
        }
    };

    private final Supplier<ObjectMapper> mapper;
    private final Class<?> targetType;

    FeedType(final Class<?> targetType, final Supplier<ObjectMapper> mapper) {
        this.targetType = targetType;
        this.mapper = mapper;
    }

    public ObjectMapper getObjectMapper() {
        return this.mapper.get();
    }

    public Class<?> getTargetType() {
        return this.targetType;
    }

    public abstract Feed mapFeed(Object rawFeed);

    public abstract Collection<Post> mapPosts(Object rawFeed);
}
