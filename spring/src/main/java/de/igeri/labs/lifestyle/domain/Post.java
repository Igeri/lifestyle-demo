package de.igeri.labs.lifestyle.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.builder.CompareToBuilder;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.net.URI;
import java.sql.Date;

@Data
@EqualsAndHashCode(exclude = {"id", "feed"})
@Entity
@NoArgsConstructor
public class Post implements Comparable<Post> {

    @Id
    @GeneratedValue
    private Long id;

    private String title;
    private String guid;
    private URI link;
    private Date dop;

    @ManyToOne
    private Feed feed;

    @Override
    public int compareTo(Post other) {
        return new CompareToBuilder()
            .append(other.dop, this.dop)
            .toComparison();
    }

}
