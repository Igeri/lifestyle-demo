package de.igeri.labs.lifestyle.domain;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface PostRepository extends JpaRepository<Post,Long>{
    Collection<Post> findByFeedId(long feedId);

    void deleteByFeedId(long id);
}
