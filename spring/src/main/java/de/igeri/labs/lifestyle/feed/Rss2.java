package de.igeri.labs.lifestyle.feed;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import lombok.Data;

import java.net.URI;

@Data
public class Rss2 {

    private Rss2.Channel channel;

    @Data
    public static class Channel {
        private String title;
        private String description;
        // TODO check how the <atom:link>-tag can be omitted
        private URI link;
//        private LocalDateTime lastBuildDate;
        private String generator;
        private Rss2.Image image;
        private String language;
        @JacksonXmlProperty(localName = "item")
        @JacksonXmlElementWrapper(useWrapping = false)
        private Rss2.Item[] items;
    }

    @Data
    public static class Image {
        private URI url;
        private String title;
        private URI link;
        private String description;
    }

    @Data
    public static class Item {
        private String title;
        private URI link;
        private String description;
        private URI comments;
//        private LocalDateTime pubDate;
        private String guid;
    }

}
