package de.igeri.labs.lifestyle.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.igeri.labs.lifestyle.domain.Feed;
import de.igeri.labs.lifestyle.domain.FeedRepository;
import de.igeri.labs.lifestyle.domain.FeedType;
import de.igeri.labs.lifestyle.domain.Post;
import de.igeri.labs.lifestyle.domain.PostRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Scope;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.xml.MappingJackson2XmlHttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Collection;
import java.util.Collections;

@Service
@Scope("singleton")
@Slf4j
public class FeedService {

    private final FeedRepository feedRepository;
    private final PostRepository postRepository;
    private final RestTemplate restTemplate;

    public FeedService(final FeedRepository feedRepository, final PostRepository postRepository,
                       final RestTemplate restTemplate) {
        this.feedRepository = feedRepository;
        this.postRepository = postRepository;
        this.restTemplate = restTemplate;
    }

    public Collection<Feed> list() {
        FeedService.log.debug("Listing all feeds...");
        return this.feedRepository.findAll();
    }

    public Collection<Post> list(final long feedId) {
        FeedService.log.debug("Listing all feed {} posts...", feedId);
        return this.postRepository.findByFeedId(feedId);
    }

    public boolean addFeed(final String url, final FeedType feedType)
        throws MalformedURLException, URISyntaxException {

        FeedService.log.debug("Adding feed '{}' as {}...", url, feedType);

        if (url == null) {
            throw new IllegalArgumentException("Feed URL must not be null!");
        }
        if (feedType == null) {
            throw new IllegalArgumentException("Feed type must not be null!");
        }

        final ObjectMapper objectMapper = feedType.getObjectMapper();
        final HttpMessageConverter<?> messageConverter = new MappingJackson2XmlHttpMessageConverter(objectMapper);
        this.restTemplate.setMessageConverters(Collections.singletonList(messageConverter));

        final Class<?> targetType = feedType.getTargetType();
        final URL targetUrl = new URL(url);
        final URI uri = targetUrl.toURI();
        final Object rawFeed = this.restTemplate.getForObject(uri, targetType);

        if (rawFeed != null) {
            final Feed feed = feedType.mapFeed(rawFeed);
            final Feed savedFeed = this.feedRepository.save(feed);

            final Collection<Post> posts = feedType.mapPosts(rawFeed);
            posts.forEach(post -> {
                post.setFeed(savedFeed);
                this.postRepository.save(post);
            });
        }

        return rawFeed != null;
    }

    public void remove(final long feedId) {
        FeedService.log.debug("Removing feed with id {}", feedId);

        this.postRepository.findByFeedId(feedId).forEach(this.postRepository::delete);
        this.feedRepository.delete(feedId);
    }

}