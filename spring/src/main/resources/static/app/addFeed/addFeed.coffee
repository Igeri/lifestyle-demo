mod = angular.module 'AddFeedModule', []


mod.controller 'AddFeedController', [
    '$scope', 'FeedService',
    ($scope, FeedService) ->
        $scope.feedUrl = ''

        $scope.addFeed = () ->
            FeedService.addFeed $scope.feedUrl
            $scope.feedUrl = ''
]


mod.directive 'addFeed', () ->
    {
        restrict: 'E'
        templateUrl: 'app/addFeed/addFeed.html'
        controller: 'AddFeedController'
    }
