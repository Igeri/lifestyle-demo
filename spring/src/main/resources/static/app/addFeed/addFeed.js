// Generated by CoffeeScript 1.12.2
(function() {
  var mod;

  mod = angular.module('AddFeedModule', []);

  mod.controller('AddFeedController', [
    '$scope', 'FeedService', function($scope, FeedService) {
      $scope.feedUrl = '';
      return $scope.addFeed = function() {
        FeedService.addFeed($scope.feedUrl);
        return $scope.feedUrl = '';
      };
    }
  ]);

  mod.directive('addFeed', function() {
    return {
      restrict: 'E',
      templateUrl: 'app/addFeed/addFeed.html',
      controller: 'AddFeedController'
    };
  });

}).call(this);

//# sourceMappingURL=addFeed.js.map
