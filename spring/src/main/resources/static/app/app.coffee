'use strict'


app = angular.module 'app', [
    'ngResource'
    'FeedListModule'
    'FeedDetailModule'
    'AddFeedModule'
    'PostDetailModule'
]


app.factory 'FeedService', [
    '$http', '$rootScope', '$resource',
    ($http, $rootScope, $resource) ->
        Feed = $resource '/api/feeds/:feedId', {feedId: '@id'}, {save: {method: 'PUT'}}
        Post = $resource '/api/feeds/:feedId/posts/:postId', {feedId: 'feedId', postId: '@id'}

        updateFeedList = () -> $rootScope.$broadcast 'updateFeedList'
        errorCallback = (response) -> alert response.data.message

        {
            addFeed: (feedUrl) -> new Feed({url: feedUrl}).$save {}, updateFeedList, errorCallback

            removeFeed: (feed) -> new Feed(feed).$remove {}, updateFeedList, errorCallback

            getFeedList: () -> Feed.query()

            getPostList: (feed) -> Post.query {feedId: feed.id}
        }
]
