mod = angular.module 'FeedDetailModule', []


mod.controller 'FeedController', [
    '$scope', 'FeedService',
    ($scope, FeedService) ->
        $scope.showDelBtn = false
        $scope.posts = []
        $scope.feedFilter = ""

        $scope.loadFeeds = () ->
            return if ($scope.posts.length != 0)
            $scope.posts = FeedService.getPostList($scope.feed)

        $scope.removeFeed = () ->
            FeedService.removeFeed($scope.feed)
]


mod.directive 'feedDetail', () ->
    {
        restrict: 'E'
        templateUrl: 'app/feedDetail/feedDetail.html'
        controller: 'FeedController'
        scope:
            feed: '<'
    }
