mod = angular.module 'PostDetailModule', []


mod.directive 'postDetail', () ->
    {
        restrict: 'E'
        templateUrl: 'app/postDetail/postDetail.html'
        scope:
            post: '<'
    }
