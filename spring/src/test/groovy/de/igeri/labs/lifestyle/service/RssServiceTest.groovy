package de.igeri.labs.lifestyle.service

import de.igeri.labs.lifestyle.domain.FeedRepository
import de.igeri.labs.lifestyle.domain.FeedType
import de.igeri.labs.lifestyle.domain.PostRepository
import org.springframework.web.client.RestTemplate
import spock.lang.Specification
import spock.lang.Unroll

@Unroll
class RssServiceTest extends Specification {

    private FeedService testInstance
    private RestTemplate restTemplate

    def setup() {
        restTemplate = Mock(RestTemplate)
        testInstance = new FeedService(Mock(FeedRepository), Mock(PostRepository), this.restTemplate)
    }

    def 'Adding a feed with parameters (#feed, #type) throws an IAE'() {
        when:
        testInstance.addFeed(feed, type)

        then:
        thrown IllegalArgumentException

        where:
        feed               | type
        null               | null
        'http://localhost' | null
        null               | FeedType.RSS2
    }

    def 'Adding a feed with an invalid URL throws an IAE'() {
        setup:
        def url = 'malformed!'

        when:
        testInstance.addFeed(url, FeedType.RSS2)

        then:
        thrown MalformedURLException
    }

    def 'Adding a feed with an valid URL calls the URL'() {
        setup:
        def url = 'http://localhost'

        when:
        testInstance.addFeed(url, FeedType.RSS2)

        then:
        1 * restTemplate.getForObject({ URI test -> test.toString() == url }, _)
    }
}
