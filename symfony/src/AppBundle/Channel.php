<?php

namespace AppBundle;

use JMS\Serializer\Annotation as Serializer;

/**
 * Class Channel
 *
 * @package AppBundle
 * @Serializer\XmlRoot("channel")
 */
class Channel {

    /**
     * @var  String
     * @Serializer\Type("string")
     */
    public $title;
    /**
     * @var  String
     * @Serializer\Type("string")
     */
    public $description;
    /**
     * @Serializer\Exclude()
     */
    // TODO check how the <atom:link>-tag can be omitted
    public $link;
    //    public LocalDateTime lastBuildDate;
    /**
     * @var
     * @Serializer\Type("string")
     */
    public $generator;
    /**
     * @var  Image
     * @Serializer\Type("AppBundle\Image")
     */
    public $image;
    /**
     * @var
     * @Serializer\Type("string")
     */
    public $language;
    /**
     * @var  Item[]
     * @Serializer\Type("array<AppBundle\Item>")
     * @Serializer\XmlList(inline=true, entry="item")
     */
    public $items;

    function __toString() {
        return json_encode($this);
    }

}
