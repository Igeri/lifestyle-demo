<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Feed;
use AppBundle\Entity\Post;
use AppBundle\RSS2;
use AppBundle\Service\FeedService;
use Doctrine\ORM\EntityManager;
use FOS\RestBundle\Controller\FOSRestController;
use JMS\DiExtraBundle\Annotation as DI;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\Request;


class FeedController extends FOSRestController {

    /**
     * @var FeedService
     */
    private $feedService;
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @DI\InjectParams({
     *     "em" = @DI\Inject("doctrine.orm.entity_manager")
     * })
     */
    function __construct($feedService, $em) {
        $this->feedService = $feedService;
        $this->em = $em;
    }

    /**
     * Add a new feed.
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     * @ApiDoc(
     *     resource=false,
     *     description="Adds a new feed to the database.",
     *     parameters={
     *          {"name"="url", "dataType"="string", "description"="A valid URL to a feed.", "required"=true,
     *          "format"="URL"}
     *     }
     * )
     */
    public function postFeedAction(Request $request) {
        $url = $request->get('url');

        /** @var RSS2 $rawFeed */
        $rawFeed = $this->feedService->loadFeed($url, new RSS2());

        $feed = new Feed();
        $feed->setUrl($url);
        $feed->setTitle($rawFeed->channel->title);
        $feed->setType("");

        foreach ($rawFeed->channel->items as $rawPost) {
            $post = new Post();
            $post->setTitle($rawPost->title);
            $post->setLink($rawPost->link);
            $post->setGuid($rawPost->guid);
            $post->setDop($rawPost->pubDate);

            $post->setFeed($feed);

            $feed->getPosts()->add($post);
        }

        $this->em->persist($feed);
        $this->em->flush();

        $view = $this->view("Ok", 200);

        return $this->handleView($view);
    }

    /**
     * @ApiDoc(
     *     resource = true,
     *     description = "Get a list of all known feeds.",
     *     requirements = {
     *         {"name"="_format", "dataType"="string", "required"=false, "requirement"="(|json)",
     *          "description"="Resulting format. Only JSON available."}
     *     }
     * )
     */
    public function getFeedsAction() {
        $feeds = $this->getDoctrine()->getRepository('AppBundle:Feed');
        /** @var Feed[] $feedList */
        $feedList = $feeds->findAllVO();

        $view = $this->view($feedList, 200)
            ->setTemplate('feed_list.html.twig')
            ->setTemplateVar('feeds');

        return $this->handleView($view);
    }

    /**
     * @param $id
     *
     * @return \FOS\RestBundle\View\View
     * @ApiDoc(
     *     response= true,
     *     description="Deletes a feed from the database.",
     *     requirements={
     *          {"name"="id", "dataType"="integer", "required"=true}
     *     }
     * )
     */
    public function deleteFeedAction($id) {
        $feeds = $this->getDoctrine()->getRepository('AppBundle:Feed');
        $feed = $feeds->find($id);
        $this->em->remove($feed);
        $this->em->flush();

        $view = $this->view("Ok", 200);

        return $view;
    }

    /**
     * @param $id
     *
     * @return \FOS\RestBundle\View\View
     * @ApiDoc(
     *     response= true,
     *     description="Returns a feed from the database.",
     *     requirements={
     *          {"name"="id", "dataType"="integer", "required"=true}
     *     }
     * )
     */
    public function getFeedAction($id) {
        $feeds = $this->getDoctrine()->getRepository('AppBundle:Feed');
        $feed = $feeds->findVO($id);

        $view = $this->view($feed, 200);

        return $view;
    }

}
