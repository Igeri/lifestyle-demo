<?php

namespace AppBundle\Controller;

use Doctrine\ORM\EntityManager;
use FOS\RestBundle\Controller\FOSRestController;
use JMS\DiExtraBundle\Annotation as DI;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\Response;

class PostController extends FOSRestController {

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @DI\InjectParams({
     *     "em" = @DI\Inject("doctrine.orm.entity_manager")
     * })
     */
    function __construct($em) {
        $this->em = $em;
    }

    /**
     * @param $slug
     *
     * @return Response
     * @ApiDoc(
     *     resource=true,
     *     description="Get a list of the feeds posts."
     * )
     */
    public function getPostsAction($slug) {
        $posts = $this->getDoctrine()->getRepository("AppBundle:Post");
        $postList = $posts->findAllPostsByFeedIdVO($slug);

        $view = $this->view($postList, 200);

        return $this->handleView($view);
    }

    public function getPostAction($id) {
        return $this->view();
    }

}
