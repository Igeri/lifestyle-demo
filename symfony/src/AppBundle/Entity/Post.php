<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Post
 * @ORM\Table(name="post")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PostRepository")
 */
class Post {
    /**
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var string
     * @ORM\Column(name="guid", type="string", length=255, unique=true)
     */
    private $guid;

    /**
     * @var string
     * @ORM\Column(name="link", type="string", length=255)
     */
    private $link;

    /**
     * @var \DateTime
     * @ORM\Column(name="dop", type="datetime")
     */
    private $dop;

    /**
     * @var Feed
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Feed", inversedBy="posts")
     * @ORM\JoinColumn(name="feed_id", referencedColumnName="id")
     */
    private $feed;


    /**
     * Get id
     *
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle() {
        return $this->title;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Post
     */
    public function setTitle($title) {
        $this->title = $title;

        return $this;
    }

    /**
     * Get guid
     *
     * @return string
     */
    public function getGuid() {
        return $this->guid;
    }

    /**
     * Set guid
     *
     * @param string $guid
     *
     * @return Post
     */
    public function setGuid($guid) {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get link
     *
     * @return string
     */
    public function getLink() {
        return $this->link;
    }

    /**
     * Set link
     *
     * @param string $link
     *
     * @return Post
     */
    public function setLink($link) {
        $this->link = $link;

        return $this;
    }

    /**
     * Get dop
     *
     * @return \DateTime
     */
    public function getDop() {
        return $this->dop;
    }

    /**
     * Set dop
     *
     * @param \DateTime $dop
     *
     * @return Post
     */
    public function setDop($dop) {
        $this->dop = $dop;

        return $this;
    }

    /**
     * @return Feed
     * @return Feed
     */
    public function getFeed() {
        return $this->feed;
    }

    /**
     * @param Feed $feed
     */
    public function setFeed($feed) {
        $this->feed = $feed;
    }

}

