<?php

namespace AppBundle;

use JMS\Serializer\Annotation as Serializer;

class Image {

    /**
     * @var
     * @Serializer\Type("string")
     */
    public $url;
    /**
     * @var
     * @Serializer\Type("string")
     */
    public $title;
    /**
     * @var
     * @Serializer\Type("string")
     */
    public $link;
    /**
     * @var
     * @Serializer\Type("string")
     */
    public $description;

    function __toString() {
        return json_encode($this);
    }

}
