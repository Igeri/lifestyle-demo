<?php

namespace AppBundle;

use JMS\Serializer\Annotation as Serializer;

class Item {
    /**
     * @var
     * @Serializer\Type("string")
     */
    public $title;
    /**
     * @var
     * @Serializer\Type("string")
     */
    public $link;
    /**
     * @var
     * @Serializer\Type("string")
     */
    public $description;
    /**
     * @var
     * @Serializer\Type("string")
     */
    public $comments;
    /**
     * @var
     * @Serializer\Type("DateTime<'D, d M Y H:i:s O'>")
     * @Serializer\SerializedName("pubDate")
     */
    public $pubDate;
    /**
     * @var
     * @Serializer\Type("string")
     */
    public $guid;
}
