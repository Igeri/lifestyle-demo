<?php

namespace AppBundle;

use JMS\Serializer\Annotation as Serializer;

/**
 * Class RSS2
 *
 * @package AppBundle
 * @Serializer\XmlRoot("rss")
 */
class RSS2 implements FeedType {

    /**
     * @var  Channel
     * @Serializer\Type("AppBundle\Channel")
     */
    public $channel;

    function __toString() {
        return json_encode($this);
    }

}
