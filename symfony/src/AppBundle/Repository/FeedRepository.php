<?php

namespace AppBundle\Repository;

/**
 * FeedRepository
 */
class FeedRepository extends \Doctrine\ORM\EntityRepository {

    /**
     * @return array<FeedVO>
     */
    public function findAllVO() {
        $dql = 'SELECT NEW AppBundle\Repository\FeedVO(f.id, f.title, f.url, f.type) 
                FROM AppBundle:Feed f';
        $query = $this->getEntityManager()->createQuery($dql);

        return $query->getResult();
    }

    /**
     * @param integer $id The ID of the feed.
     *
     * @return FeedVO
     */
    public function findVO($id) {
        $dql = 'SELECT NEW AppBundle\Repository\FeedVO(f.id, f.title, f.url, f.type) 
                FROM AppBundle:Feed f
                WHERE f.id = :id';
        $query = $this->getEntityManager()->createQuery($dql);
        $query->setParameter('id', $id);

        return $query->getSingleResult();
    }
}
