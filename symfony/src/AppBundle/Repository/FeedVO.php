<?php

namespace AppBundle\Repository;


class FeedVO {

    private $id;
    private $title;
    private $link;
    private $type;

    function __construct($id, $title, $link, $type) {
        $this->id = $id;
        $this->title = $title;
        $this->link = $link;
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getTitle() {
        return $this->title;
    }

    /**
     * @return mixed
     */
    public function getLink() {
        return $this->link;
    }

    /**
     * @return mixed
     */
    public function getType() {
        return $this->type;
    }

}
