<?php

namespace AppBundle\Repository;

/**
 * PostRepository
 */
class PostRepository extends \Doctrine\ORM\EntityRepository {

    public function findAllPostsByFeedIdVO($id) {
        $dql = 'SELECT NEW AppBundle\Repository\PostVO(p.id, p.title, p.guid, p.link, p.dop, f.id) 
                FROM AppBundle:Post p JOIN AppBundle:Feed f 
                WHERE f.id = :id';
        $query = $this->getEntityManager()->createQuery($dql);
        $query->setParameter('id', $id);

        return $query->getResult();
    }
}
