<?php

namespace AppBundle\Repository;


class PostVO {

    private $id;
    private $title;
    private $guid;
    private $link;
    private $dop;
    private $feedId;

    /**
     * PostVO constructor.
     *
     * @param $id
     * @param $title
     * @param $guid
     * @param $link
     * @param $dop
     * @param $feedId
     */
    public function __construct($id, $title, $guid, $link, $dop, $feedId) {
        $this->id = $id;
        $this->title = $title;
        $this->guid = $guid;
        $this->link = $link;
        $this->dop = $dop;
        $this->feedId = $feedId;
    }

    /**
     * @return mixed
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getTitle() {
        return $this->title;
    }

    /**
     * @return mixed
     */
    public function getGuid() {
        return $this->guid;
    }

    /**
     * @return mixed
     */
    public function getLink() {
        return $this->link;
    }

    /**
     * @return mixed
     */
    public function getDop() {
        return $this->dop;
    }

    /**
     * @return mixed
     */
    public function getFeedId() {
        return $this->feedId;
    }

}
