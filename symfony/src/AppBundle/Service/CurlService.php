<?php

namespace AppBundle\Service;

use JMS\DiExtraBundle\Annotation as DI;
use JMS\Serializer\SerializerBuilder;

/**
 * Class CurlService
 *
 * @package AppBundle\Service
 * @return Object of type $type.
 * @DI\Service("curl_service")
 */
class CurlService {

    public function query($url, $type) {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $xmlData = curl_exec($curl);

        $xmlData = preg_replace("/<!--.*?-->/", "", $xmlData);

        $serializer = SerializerBuilder::create()->build();
        $data = $serializer->deserialize($xmlData, $type, 'xml');

        return $data;
    }

}
