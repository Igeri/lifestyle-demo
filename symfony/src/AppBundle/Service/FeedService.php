<?php

namespace AppBundle\Service;

use AppBundle\FeedType;
use JMS\DiExtraBundle\Annotation as DI;
use Monolog\Logger;

/**
 * @DI\Service("feed_service")
 */
class FeedService {

    /** @var  Logger */
    private $log;
    /** @var  CurlService */
    private $curlService;

    /**
     * FeedService constructor.
     * @DI\InjectParams({
     *     "log" = @DI\Inject("logger")
     * })
     */
    function __construct($log, $curlService) {
        $this->log = $log;
        $this->curlService = $curlService;
    }

    /**
     * @param          $url
     * @param FeedType $feedType
     *
     * @return mixed
     */
    public function loadFeed($url, FeedType $feedType) {
        $feed = $this->curlService->query($url, 'AppBundle\RSS2');

        return $feed;
    }

}
