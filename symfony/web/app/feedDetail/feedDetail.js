// Generated by CoffeeScript 1.12.2
(function() {
  var mod;

  mod = angular.module('FeedDetailModule', []);

  mod.controller('FeedController', [
    '$scope', 'FeedService', function($scope, FeedService) {
      $scope.showDelBtn = false;
      $scope.posts = [];
      $scope.feedFilter = "";
      $scope.loadFeeds = function() {
        if ($scope.posts.length !== 0) {
          return;
        }
        return $scope.posts = FeedService.getPostList($scope.feed);
      };
      return $scope.removeFeed = function() {
        return FeedService.removeFeed($scope.feed);
      };
    }
  ]);

  mod.directive('feedDetail', function() {
    return {
      restrict: 'E',
      templateUrl: 'app/feedDetail/feedDetail.html',
      controller: 'FeedController',
      scope: {
        feed: '='
      }
    };
  });

}).call(this);

//# sourceMappingURL=feedDetail.js.map
