mod = angular.module 'FeedListModule', []


mod.controller 'FeedListController', [
    '$scope', 'FeedService',
    ($scope, FeedService) ->
        updateList = () -> $scope.feeds = FeedService.getFeedList()

        $scope.$on 'updateFeedList', updateList

        updateList()
]
